<?php

namespace Drupal\exif\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class ExifSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'exif.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'exif_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(static::SETTINGS);

    $form['exif'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable EXIF',
    '#required' => FALSE,
    '#default_value' => $config->get('exif'),
  );

  $form['mediatypes'] = array(
    '#type' => 'textarea',
    '#title' => 'Media types',
    '#required' => FALSE,
    '#default_value' => $config->get('mediatypes'),
    '#description' => 'add media types for each line one media type'
  );

  $form['camera'] = array(
    '#type' => 'textfield',
    '#title' => 'Field for Camera',
    '#required' => FALSE,
    '#default_value' => $config->get('camera'),
  );

  $form['shutterspeed'] = array(
    '#type' => 'textfield',
    '#title' => 'Field for shutterspeed',
    '#required' => FALSE,
    '#default_value' => $config->get('shutterspeed'),
  );

  $form['iso'] = array(
    '#type' => 'textfield',
    '#title' => 'Field for iso',
    '#required' => FALSE,
    '#default_value' => $config->get('iso'), 
  );

  $form['lens'] = array(
    '#type' => 'textfield',
    '#title' => 'Field for lens',
    '#required' => FALSE,
    '#default_value' => $config->get('lens'),
  );
  
  $form['focallength'] = array(
    '#type' => 'textfield',
    '#title' => 'Field for focallength',
    '#required' => FALSE,
    '#default_value' => $config->get('focallength'),
  );
  
  $form['f'] = array(
    '#type' => 'textfield',
    '#title' => 'Field for f',
    '#required' => FALSE,
    '#default_value' => $config->get('f'),
  );
  
  $form['flash'] = array(
    '#type' => 'textfield',
    '#title' => 'Field for flash',
    '#required' => FALSE,
    '#default_value' => $config->get('flash'),
  );
  return parent::buildForm($form, $form_state);
}


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('exif', $form_state->getValue('exif'))
      ->set('mediatypes', $form_state->getValue('mediatypes'))
      ->set('camera', $form_state->getValue('camera'))
      ->set('shutterspeed', $form_state->getValue('shutterspeed'))
	  ->set('iso', $form_state->getValue('iso'))
	  ->set('lens', $form_state->getValue('lens'))
	  ->set('focallength', $form_state->getValue('focallength'))
	  ->set('f', $form_state->getValue('f'))
	  ->set('flash', $form_state->getValue('flash'))
      ->save();

    parent::submitForm($form, $form_state);
  }


}
